Unpack the package Pretius_zadanie_rekrutacja.zip

Go to commandline in the main project directory:

Maven dependency update:

mvn clean install -U

Running as a Packaged Application:

java -jar target/zadanie-0.0.1-SNAPSHOT.jar

Go to POSTMAN(toolchain for API developers):

Method: POST 
url: localhost:8080/

Body:
choose from the list "JSON (application/json)"

insert sample data:

{
	"amount" : 10,
	"currencyAmount" : "PLN",
	"currencyTargetAmount" : "EUR"
}

push button "send"
